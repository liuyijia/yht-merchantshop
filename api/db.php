<?php
include '../data/config.php';

$db_host_arr = explode(':', $db_host);
$_host = $db_host_arr[0];
$_port = isset($db_host_arr[1]) ? $db_host_arr[1] : 3306;

$dsn = "mysql:host={$_host};port={$_port};dbname={$db_name}";
try {
    $pdo = new PDO($dsn, $db_user, $db_pass);
} catch (PDOException $e) {
    $pdo = null;
    echo json_encode(array('err_code'=>101, 'msg'=>'数据库连接失败'));
    exit();
}

function sql_query_one($pdo, $sql, $param=array()){
    $stmt = $pdo->prepare($sql);
    $stmt->execute($param);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    return $data;
}

function sql_query_all($pdo, $sql, $param=array()){
    $stmt = $pdo->prepare($sql);
    $stmt->execute($param);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $data;
}

function sql_insert($pdo, $sql, $param=array()){

}