<?php
include 'common.php';

if(empty($_GET)){
    echo json_encode(array('err_code'=>102));

}else{
    include './db.php';

    if($pdo){
        $sql = 'SELECT A.order_id, A.order_status, A.shipping_status, A.pay_status, A.log_time updated_time FROM '.$prefix.'order_action A ';

        $param = array();
        if(isset($_GET['order_id'])){
            $sql .= 'WHERE A.order_id=?';
            $param = array($_GET['order_id']);

        }elseif(isset($_GET['updated_time'])){
            $sql .= 'WHERE A.log_time>?';
            $param = array($_GET['updated_time']);
        }

        $data = sql_query_all($pdo, $sql, array($_GET['updated_time']));

        if(false === $data){
            echo json_encode(array('err_code'=>-1));    // 数据库查询出错
        } else {
            echo json_encode(array('err_code'=>0, 'data'=>$data));
        }

    }else{
        echo json_encode(array('err_code'=>101));
    }

}